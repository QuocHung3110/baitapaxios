// Quy ước
// pass = true,
// not pass = false,

// kiểm tra rỗng
function ktrRong(value,span){
    if(value==''||value==0){
        document.getElementById(span).innerHTML='Vui lòng điền/chọn dữ liệu'
        return false;
    }else{
        document.getElementById(span).innerHTML=``;
        return true;   
    };    
};

// kiểm tra trùng tài khoản
function ktrTrung(taiKhoan,span,DSND){
    // findIndex trả về vị trí của item nếu điều kiện true, nếu ko tìm thấy trả về -1
    var index=DSND.findIndex(function(item){
        return item.taiKhoan == taiKhoan;
    });
    if (index!=-1){
        document.getElementById(span).innerHTML=`Tài khoản trùng, vui lòng chọn tài khoản khác.`;
        return false;
    }else{
        document.getElementById(span).innerHTML=``;
        return true;
    };
};

// kiểm tra họ tên 
function ktrTen(hoTen,span){
    const re =
    /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
    var isValid=re.test(hoTen);
    if (isValid){
        document.getElementById(span).innerHTML=``;
        return true;
    }else{
        document.getElementById(span).innerHTML='Họ tên không chứa số hoặc ký tự đặc biệt'
        return false;
    };
};

// kiểm tra email
function ktrEmail(email,span){
    const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;    
    var isValid=re.test(email);
    if (isValid){
        document.getElementById(span).innerHTML=``;
        return true;
    }else{
        document.getElementById(span).innerHTML='Email không đúng định dạng';
        return false;
    };
};

// kiểm tra mật khẩu
function ktrMatKhau(pass,span,min,max){
    const re=/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,8}$/;
    var isValid=re.test(pass);
    if (isValid==false||pass.length<min||pass.length>max){
        document.getElementById(span).innerHTML=`Mật Khẩu từ ${min}-${max} ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)`;
        return false;
    }else{
        document.getElementById(span).innerHTML=``;
        return true;
    };
};

// kiểm tra mô tả
function ktrMoTa(moTa,span,max){
    if(moTa.length>max){
        document.getElementById(span).innerHTML=`Mô tả không vượt quá ${max} ký tự`;
        return false;
    }else{
        document.getElementById(span).innerHTML=``;
        return true;
    };
}

// validate thêm
function validation(user){
    var isValid=true;
    // ktr rỗng
    isValid=
        ktrRong(user.taiKhoan,'spanTaiKhoan') &
        ktrRong(user.hoTen,'spanHoTen') &         
        ktrRong(user.email,'spanEmail') &         
        ktrRong(user.matKhau,'spanMatKhau') &         
        ktrRong(user.hinhAnh,'spanHinhAnh') &         
        ktrRong(user.loaiND,'spanloaiNguoiDung') &         
        ktrRong(user.ngonNgu,'spanloaiNgonNgu') &         
        ktrRong(user.moTa,'spanMoTa') 
    ;
    if (isValid){
        // ktra trùng tài khoản
        isValid=ktrTrung(user.taiKhoan,'spanTaiKhoan',DSND);
        // // ktra tên , email, ngày làm
        isValid=isValid & 
            ktrTen(user.hoTen,'spanHoTen') & 
            ktrEmail(user.email,'spanEmail') & 
            ktrMatKhau(user.matKhau,'spanMatKhau',6,8) &
            ktrMoTa(user.moTa,'spanMoTa',60);
    };
    return isValid;    
};

// validate cập nhật
function validationCapNhat(user,account){
    var isValid=true;
    // ktr rỗng
    isValid=
        ktrRong(user.taiKhoan,'spanTaiKhoan') &
        ktrRong(user.hoTen,'spanHoTen') &         
        ktrRong(user.email,'spanEmail') &         
        ktrRong(user.matKhau,'spanMatKhau') &         
        ktrRong(user.hinhAnh,'spanHinhAnh') &         
        ktrRong(user.loaiND,'spanloaiNguoiDung') &         
        ktrRong(user.ngonNgu,'spanloaiNgonNgu') &         
        ktrRong(user.moTa,'spanMoTa') 
    ;
    if (isValid){
        // ktra trùng tài khoản
        if (user.taiKhoan!=account){
            isValid=ktrTrung(user.taiKhoan,'spanTaiKhoan',DSND);
        };     
        // // ktra tên , email, ngày làm
        isValid=isValid & 
            ktrTen(user.hoTen,'spanHoTen') & 
            ktrEmail(user.email,'spanEmail') & 
            ktrMatKhau(user.matKhau,'spanMatKhau',6,8) &
            ktrMoTa(user.moTa,'spanMoTa',60);
    };
    return isValid;    
}